# Copyright 2011 Dan Callaghan <djc@djc.id.au>
# Copyright 2022 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true vala_dep=true ]
require meson

SUMMARY="Utility library that aims to ease the handling and implementation of UPnP A/V profiles"
HOMEPAGE="https://wiki.gnome.org/Projects/GUPnP"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc

    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.36.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.58.0]
        dev-libs/libxml2:2.0
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'vapi'
)

pkg_setup() {
    vala_pkg_setup
    meson_pkg_setup
}

